import './App.css';
import LoginProvider from './LoginComponents/LoginProvider';


function App() {
  return (
    <div className="App">
      <header className="App-header">
          <LoginProvider />  
      </header>
    </div>
  );
}

export default App;
