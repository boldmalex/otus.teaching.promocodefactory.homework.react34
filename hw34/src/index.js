import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from 'react-redux';
import {loginStore} from './LoginComponents/loginStore'



ReactDOM.render(
    <Provider store ={loginStore}>
        <App />
    </Provider>,
    document.getElementById('root')
);

