import React from "react";
import {connect} from 'react-redux';


const CurrentUser = (props) => {
    return(
            <div style={{width:'100%',display:'flex'}}>
                <h3>Hello, {props.loginInfo.userName}!</h3> 
                <a href='#' onClick={logout}> Logout</a>
            </div>
    )

    function logout(){
        console.log('logout');
        props.logout();
    }
}


const mapState = (state) =>{
    return {
        loginInfo: state
    }
}

const mapDispath = (dispatch) =>{
    return{
        logout: () => dispatch({type: 'LOGOUT'})
    }
}

export default connect(mapState,mapDispath)(CurrentUser)