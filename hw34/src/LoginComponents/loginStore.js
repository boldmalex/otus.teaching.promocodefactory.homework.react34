import {createStore} from 'redux';
import StateLoader from '../State/StateLoader';

const stateKey = "auth";
const stateLoader = new StateLoader();

const reducer = (state = {isAuthenticated: false, userName: ''}, action) =>{
    if (action.type === 'LOGIN'){
        return {isAuthenticated: action.payload.isAuthenticated, userName: action.payload.userName}
    }
    
    if (action.type === 'LOGOUT'){
        return {isAuthenticated: false, userName: ''}
    }

    return state;
}

export const loginStore = createStore(reducer, stateLoader.loadState(stateKey));

loginStore.subscribe(()=>{
    stateLoader.saveState(stateKey, loginStore.getState());
});




