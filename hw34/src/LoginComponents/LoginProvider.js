import React,{useState} from "react";
import '../Styles/LoginForm.css'
import {connect} from 'react-redux';
import ContentProvider from '../Content/ContentProvider'


// Use Test/Test for login/password
const LoginProvider = (props) =>{

    const [errorMessage, setErrorMessage] = useState('')


    function apiLogin (userName, userPassword) {
        if (userName === 'Test' && userPassword === 'Test')
        {
            props.login(true, userName);
            setErrorMessage('');
        }
        else
        {
            setErrorMessage('invalid login or pass');   
        }
    }


    function handleSubmit(e){
        e.preventDefault();
        console.log("handle submit");
        let userName = e.target.elements.UserName.value
        let userPassword = e.target.elements.UserPass.value
        apiLogin(userName, userPassword);
    }


    if (props.loginInfo.isAuthenticated){
        return (<ContentProvider />);
    }
    else{
        return(
            <div>
                <p style={{ color: 'red' }}>{errorMessage}</p>
                <form onSubmit={handleSubmit}>
                    <input type='text' name="UserName" placeholder='Имя пользователя' required></input>
                    <input type='password' name="UserPass" placeholder='Пароль' required></input>
                    <input type='submit'></input>
                </form>
            </div>
        );
    }
}

const mapState = (state) =>{
    return {
        loginInfo: state
    }
}

const mapDispath = (dispatch) =>{
    return{
        login: (isAuthenticated, userName) => dispatch({type: 'LOGIN', payload: {isAuthenticated:isAuthenticated, userName:userName}})
    }
}

export default connect(mapState,mapDispath)(LoginProvider)