class StateLoader {

    loadState(key) {
        try {
            let serializedState = localStorage.getItem(key);

            if (serializedState === null) {
                return this.initializeState();
            }

            return JSON.parse(serializedState);
        }
        catch (err) {
            return this.initializeState();
        }
    }

    saveState(key, state) {
        try {
            let serializedState = JSON.stringify(state);
            localStorage.setItem(key, serializedState);

        }
        catch (err) {
        }
    }

    initializeState() {
        return {
                sAuthenticated: false, userName: ''
        }
    }
}

export default StateLoader;