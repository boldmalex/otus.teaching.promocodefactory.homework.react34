import React,{useState} from "react";
import './ContentProvider.css'
import HomePage from './HomePage'
import AboutPage from './AboutPage'
import ErrorPage from './ErrorPage'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom"; 
import CurrentUser from "../LoginComponents/CurrentUser";


const ContentProvider = () => {

    return(
        <div>
            <CurrentUser/>
            <Router>
                <ul>
                    <li>
                        <Link to="/Home">Home</Link>
                    </li>
                    <li>
                        <Link to="/About">About</Link>
                    </li>
                </ul>
                <Switch>
                    <Route exact path="/Home">
                        <HomePage/>
                    </Route>
                    <Route path="/About">
                        <AboutPage/>
                    </Route>
                    <Route path="*">
                        <ErrorPage/>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default ContentProvider;